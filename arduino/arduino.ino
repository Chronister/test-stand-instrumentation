#include "channels.h"
#include <Wire.h>
#include <Adafruit_ADS1015.h>
#include <OneWire.h>

typedef uint8_t scratch_pad[9];
typedef uint8_t device_address[9];

// Class to interface with the pressure sensor ADC
Adafruit_ADS1115 ads; 

// Class to interface with the thermocouples
#define ONE_WIRE_BUS 7
OneWire onewire(ONE_WIRE_BUS);

// Thermocouple ADC variables. scratch_pad is defined above.
scratch_pad ScratchPad;
int16_t RawTemps[TEMP_CHANNELS];
int16_t InternalTemps[TEMP_CHANNELS];
int KnownDevices = 0;
uint8_t CurrentDevice = 0;

// OneWire commands
#define STARTCONVO      0x44  // Tells device to take a temperature reading and put it on the scratchpad
#define COPYSCRATCH     0x48  // Copy EEPROM
#define READSCRATCH     0xBE  // Read EEPROM
#define WRITESCRATCH    0x4E  // Write to EEPROM
#define RECALLSCRATCH   0xB8  // Reload from last known

// Scratchpad locations
#define TEMP_LSB        0
#define TEMP_MSB        1
#define INT_TEMP_LSB 	2
#define INT_TEMP_MSB 	3
#define CONFIGURATION   4
#define RESERVED_1 		5
#define RESERVED_2 		6
#define RESERVED_3		7
#define SCRATCHPAD_CRC  8

#define TEMPERATURE_PRECISION 9
// Milliseconds at last temperature poll
unsigned long mLastTemp = 0;

// Everything to do with the thermocouple ADCs. 
namespace tc 
{
	bool validAddress(uint8_t* DeviceAddress)
	{
	  return (onewire.crc8(DeviceAddress, 7) == DeviceAddress[7]);
	}

	bool getAddress(uint8_t* DeviceAddress, uint8_t index)
	{
		uint8_t depth = 0;
		onewire.reset_search();
		while (depth <= index && onewire.search(DeviceAddress))
		{
			if (depth == index && validAddress(DeviceAddress)) return true;
			depth++;
		}

	return false;
	}

// Turns out we never actually need to write the whole scratch pad to it.
#if 0
	void writeScratchPad(uint8_t* DeviceAddress, const uint8_t* ScratchPad)
	{
		onewire.reset();
		onewire.select(DeviceAddress);
		onewire.write(WRITESCRATCH);
		onewire.write(ScratchPad[LOW_ALARM_TEMP]); // low alarm temp
		// DS18S20 does not use the configuration register
		if (DeviceAddress[0] != DS18S20MODEL) onewire.write(ScratchPad[CONFIGURATION]); // configuration
		onewire.reset();
		// save the newly written values to eeprom
		onewire.write(COPYSCRATCH, parasite);
		if (parasite) delay(10); // 10ms delay
		onewire.reset();
	}
#endif 

	void readScratchPad(uint8_t* DeviceAddress, uint8_t* ScratchPad)
	{
	  onewire.reset();
	  onewire.select(DeviceAddress);
	  onewire.write(READSCRATCH);

	  // byte 0: temperature LSB and fault status
	  // byte 1: temperature MSB
	  // byte 2: internal temp LSB
	  // byte 3: internal temp MSB
	  // byte 4: configuration register
	  // byte 5: RESERVED
	  // byte 6: RESERVED
	  // byte 7: RESERVED
	  // byte 8: CRC
	  //
		for(int i=0; i<9; i++)
		{
			ScratchPad[i] = onewire.read();
		}

		for (uint8_t i=0; i<8; i++) {
			//Serial.print("\n 0x"); Serial.print(ScratchPad[i], HEX);
		}
		onewire.reset();
	}

	// Tells the ADCs to start preparing a value for us. Takes 72 milliseconds after this happens to get a reading.
	void startConversion(uint8_t* DeviceAddress)
	{
		onewire.reset();
		onewire.select(DeviceAddress);
		onewire.write(STARTCONVO, false);
	}	

	void begin()
	{
		uint8_t tempDA[8];
		onewire.reset_search();
		while (onewire.search(tempDA))
		{
			if (validAddress(tempDA))
			{
				readScratchPad(tempDA, ScratchPad);
				//bitResolution = max(bitResolution, getResolution(DeviceAddress));
				++KnownDevices;
			}
		}
		// Serial.print("Found ");
		// Serial.print(KnownDevices);
		// Serial.print(" devices.\n");
	}
}

void setup()
{
	Serial.begin(9600);
	tc::begin();
	ads.begin();
}

void loop()
{
#if 1 // Onboard analog 
	// LOAD_CHANNELS is most likely always 1. But just in case.
	for (int a = 0; a < LOAD_CHANNELS; ++a)
	{
		Serial.print(analogRead(A0 + a));
		Serial.print(",");
	}
#endif

#if 1 // Pressure ADC
	for (int p = 0; p < PRES_CHANNELS; ++p)
	{
		Serial.print(ads.readADC_Differential_0_1());
		Serial.print(",");
	}
#endif

#if 1 // Thermocouple ADCs
	// There are (at least) two ways to poll this:
	//  1: All at once, after polling the analog sensors. This is slow and will
	//  introduce delay to the whole system.
	//  2: One sensor per analog poll. This means that at any given time, data from
	//  4 of the channels are stale. However, we can poll e.g. the load cell more
	//  frequently.

	// Note: this is actually not the most efficient way to do this, I think. Each device needs to wait 72 ms, but the
	// delay between switching devices is much shorter. It might be possible to query all of the devices at once with a
	// microsecond-factor delay between devices to account for onewire signalling.
	if (millis() - mLastTemp > 72)
	{
		device_address DeviceAddress;
		if (tc::getAddress(DeviceAddress, CurrentDevice))
		{
			tc::readScratchPad(DeviceAddress, ScratchPad);

			RawTemps[CurrentDevice] = (((int16_t)ScratchPad[TEMP_MSB]) << 8) | ScratchPad[TEMP_LSB];
			RawTemps[CurrentDevice] >>= 2;
			// These devices have an internal thermocouple too, but we don't need to read those right now.
			// InternalTemps[CurrentDevice] = (((int16_t)ScratchPad[INT_TEMP_MSB]) << 8) | ScratchPad[INT_TEMP_LSB];
			// InternalTemps[CurrentDevice] >>= 4;

			mLastTemp = millis();
			CurrentDevice = (++CurrentDevice) % TEMP_CHANNELS;
			tc::startConversion(DeviceAddress);
		}
		else
		{
			// Couldn't poll one of the TCs, skip it and move on.
			CurrentDevice = (++CurrentDevice) % TEMP_CHANNELS;
		}
	}
	
	for (int t = 0; t < TEMP_CHANNELS; ++t)
	{
		Serial.print(RawTemps[t]);
		Serial.print(",");
		// Note that the conversion factor for internal temperatures is different -- they're higher resolution but have
		// less range.
		//Serial.print(InternalTemps[t] * 0.0625f);
		//Serial.print(",");
	}
#endif

	Serial.println();
}
