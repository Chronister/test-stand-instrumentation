
// Controls what is output to the .log file and to the command line
#define LOG_LEVEL plat::LOG_WARNING

#include "chr_linuxplatform.h"

// Headers needed for interneting
#include <termios.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
// Header needed for threading
#include <pthread.he>

// These definitions are for convenience and to implement function signatures defined elsewhere in chr_ headers.
void* Alloc(size_t BytesToAlloc, bool32 ZeroTheMemory) { return plat::Alloc(BytesToAlloc, ZeroTheMemory); }
bool32 Free(void* Memory) { return plat::Free(Memory); } 

// Define the set of bits that means NAN in floating point terminology
#ifndef NAN
       static const unsigned long __nan[2] = {0xffffffff, 0x7fffffff};
       #define NAN (*(const float *) __nan)
#endif

#undef Assert /* Defined earlier in chr.h */
#define Assert(Statement) !(Statement) && (LogF(plat::LOG_ERROR, "!! ASSERTION FAILED !! " #Statement " at " __FILE__ ": %d\n", __LINE__)); 

typedef int HANDLE; // So we can deal with linux/windows files monotonically in serialread.cpp

#if !defined(SOCKET)
	#define SOCKET int 
#endif

#define BAUD 9600 /* Of the arduino */

// Need to be able to refer to the windows and linux functions the same way
void
Sleep(uint32 Milliseconds) { usleep(Milliseconds * 1000); }

int closesocket(int s)
{
	return close(s);
}

int
GetSocketError() 
{ 
 // TODO: Need some kind of errno.h function here
}

// On linux, sockets don't need to be initialized or cleaned up?
static bool
InitializeSockets() { return true; }

static void 
CleanupSockets() { return; };



// Serial port code borrowed from https://github.com/todbot/arduino-serial
// Modified for our specific purposes. 
HANDLE
OpenSerialPort(char* PortName)
{
    struct termios toptions;
    HANDLE fd;
    
    fd = open(PortName, O_RDWR | O_NONBLOCK );
    
    if (fd == -1)  {
        LogF(plat::LOG_ERROR, "Unable to open port %s\n", PortName);
        return -1;
    }
    
    //int iflags = TIOCM_DTR;
    //ioctl(fd, TIOCMBIS, &iflags);     // turn on DTR
    //ioctl(fd, TIOCMBIC, &iflags);    // turn off DTR

    if (tcgetattr(fd, &toptions) < 0) {
        LogF(plat::LOG_ERROR, "Unable to get %s attributes.\n", PortName);
        return -1;
    }
    speed_t brate = BAUD; // let you override switch below if needed
    
    cfsetispeed(&toptions, brate);
    cfsetospeed(&toptions, brate);

    // 8N1
    toptions.c_cflag &= ~PARENB;
    toptions.c_cflag &= ~CSTOPB;
    toptions.c_cflag &= ~CSIZE;
    toptions.c_cflag |= CS8;
    // no flow control
    toptions.c_cflag &= ~CRTSCTS;

    //toptions.c_cflag &= ~HUPCL; // disable hang-up-on-close to avoid reset

    toptions.c_cflag |= CREAD | CLOCAL;  // turn on READ & ignore ctrl lines
    toptions.c_iflag &= ~(IXON | IXOFF | IXANY); // turn off s/w flow ctrl

    toptions.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG); // make raw
    toptions.c_oflag &= ~OPOST; // make raw

    // see: http://unixwiz.net/techtips/termios-vmin-vtime.html
    toptions.c_cc[VMIN]  = 0;
    toptions.c_cc[VTIME] = 0;
    //toptions.c_cc[VTIME] = 20;
    
    tcsetattr(fd, TCSANOW, &toptions);
    if( tcsetattr(fd, TCSAFLUSH, &toptions) < 0) {
        LogF(plat::LOG_ERROR, "Unable to set %s attributes.\n", PortName);
        return -1;
    }

    return fd;
}

// When we opened the file we should have set the option to O_APPEND
void AppendToFile(HANDLE FileHandle, string Text)
{
	write(FileHandle, Text.Value, Text.Length);
}

bool ReadFromFile(HANDLE FileHandle, char* Buffer, uint32 BytesToRead)
{
	return read(FileHandle, Buffer, BytesToRead) >= 0;
}

void CloseHandle(HANDLE FileHandle)
{
	close(FileHandle);
}

// Queue item used by the threaded queue.
struct data_queue_item
{
	bool32 Valid;
	string Data;
};

#define QUEUE_SIZE 16
struct data_queue 
{
	data_queue_item Items[QUEUE_SIZE]; 
	uint32 LastWrittenItem;
};

#include "serialread.cpp"

struct relay_params
{
	connection C;
	data_queue* DataQueue;
};

void* 
RelayDataToClients(void* RelayParamsPtr)
{
	if (RelayParamsPtr == null) { printf("Null parameter\n"); return null; }
	relay_params RelayParams = *((relay_params*)RelayParamsPtr);

	int BytesSent = 1;
	uint32 LastSentIndex = 0;
	while(BytesSent > 0)
	{
		if (RelayParams.DataQueue->LastWrittenItem != LastSentIndex)
		{
			data_queue_item* Item = &RelayParams.DataQueue->Items[RelayParams.DataQueue->LastWrittenItem];
			if (Item->Valid)
			{
				net_data Buffer;
				Buffer.Content = (uint8*)Item->Data.Value;
				Buffer.Size = Item->Data.Length + 1;
				//printf("Sent %s to client on connection %d\n", Item->Data.Value, RelayParams.C.Socket);
				BytesSent = Send(RelayParams.C, Buffer);
				LastSentIndex = RelayParams.DataQueue->LastWrittenItem;
			}
		}
	}

	return null;
}

struct server_params
{
	char* Port;
	data_queue* DataQueue;
};

void* 
ListenForClients(void* ServerParamsPtr)
{
	if (ServerParamsPtr == null) { printf("Null server parameter\n"); return null; }
	server_params* ServerParams = (server_params*)ServerParamsPtr;

	connection C = ListenOverTCP(ServerParams->Port);
	if (C.Socket == INVALID_SOCKET) { return null; }

	while (true) // TODO(chronister): condition?
	{
		connection ClientCnx = AcceptClient(C);
		if (ClientCnx.Socket == INVALID_SOCKET) { Sleep(10); continue; }

		relay_params RelayParams = { ClientCnx, ServerParams->DataQueue };
		printf("Got a client on socket %d, forking off...\n", C.Socket); 
		pthread_t ClientThread;
		pthread_create(&ClientThread, null, &RelayDataToClients, &RelayParams);
		Sleep(10); // Wait for the client to copy the variables to its own stack before continuing
	}
}


// Using LinuxMain because chr_linuxplatform.h defines its own main() so that it can do some platform-specific
// initialization. 
// Arguments: serial [SERIALPORT] [LISTENPORT]
//       e.g. serial /dev/ttyACM0 8888
int LinuxMain(int argc, char* argv[])
{
	char* Port = "8888";
	char* SerialPort = "/dev/ttyACM0";
	// if argc == 1, argv is just the name of the thing called (no args)
	if (argc >= 2) { SerialPort = argv[1]; }
	if (argc >= 3) { Port = argv[2]; }

	data_queue DataQueue = {};

	// Hooray for shared memory space between threads.
	server_params ServerParams = { Port, &DataQueue };
	pthread_t ServerThread;
	pthread_create(&ServerThread, null, &ListenForClients, &ServerParams);

#if 1
	HANDLE SerialHandle = OpenSerialPort(SerialPort);
	HANDLE DataFile = open("serial.dat", O_CREAT|O_APPEND|O_RDWR);
	if (DataFile < 0)
	{
		LogF(plat::LOG_ERROR, "Couldn't open serial.dat for writing.\n");
	}

	// In most cases this is probably going to block until the program is
	// killed.
	SerialReadWriteLoop(SerialHandle, DataFile, &DataQueue);

	close(SerialHandle);
	close(DataFile);
#else
	// This function doesn't support the server-model of sending data, so it is pretty much useless at this point.
	TestDummyData(Hostname,Port);
#endif

	return 0;
}


