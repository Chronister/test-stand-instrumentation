All of the files prefixed with chr_ are part of a personal library that I've been developing recently. I had originally
meant to fold those all into one place as much as possible and maybe lean more on the C++ standard library, but I never
got around to doing so and didn't have time to ensure stability just before the engine test.

It should be fairly obvious what is going on in serialread.cpp, but in case it isn't, all of the source files are
provided here as-is. I won't blame you if you decide to use something completely different for the final rocket, but it
may be educational to see how I was doing things here.
