#if !defined(CHR_ARRAY)

#include "chr_string.h" //TODO(chronister): Eliminate this dependency (maybe even reverse it)

void* Alloc(size_t BytesToAlloc, bool32 ZeroTheMemory);
bool32 Free(void* Memory);

#define AllocArray(T, count) (T*)Alloc((count) * sizeof(T), true)
#define AllocArrayNoClear(T, count) (T*)Alloc((count) * sizeof(T), false)

#define PushItemOntoArray(T, Capacity, NumItems, Array, Item) PushItemOntoArrayPointer(&(Capacity), &(NumItems), (void**)(&(Array)), sizeof(T), (void*)(&(Item)))
internal uint32
PushItemOntoArrayPointer(uint32* CapacityPtr, uint32* NumItemsPtr, void** ArrayPtr, uint32 ItemSize, void* Item)
{
    uint32 NumItems = *NumItemsPtr;

    if (NumItems >= *CapacityPtr)
    {
        void* OldArray = *ArrayPtr;
        uint32 NewSize = Max(NumItems + 0x10, NumItems * 2);
        *ArrayPtr = Alloc(ItemSize * (NewSize), false);
        CopyMemory(*ArrayPtr, OldArray, ItemSize * NumItems);
        Free(OldArray);
        *CapacityPtr = NewSize;
    }

    CopyMemory((char*)(*(ArrayPtr)) + NumItems*ItemSize, Item, ItemSize);
    (*NumItemsPtr)++;

    return *NumItemsPtr - 1;
}

#define RemoveItemFromArrayByIndex(T, NumItems, Array, Index) RemoveItemFromArrayPointerByIndex(&(NumItems), Array, sizeof(T), Index)
internal void
RemoveItemFromArrayPointerByIndex(uint32* NumItemsPtr, void* Array, uint32 ItemSize, uint32 ItemIndex)
{
    CopyMemory(((uint8*)Array) + (ItemSize * ItemIndex), ((uint8*)Array) + (ItemSize * (*NumItemsPtr - 1)), ItemSize);
    ZeroMemory(((uint8*)Array) + (ItemSize * (*NumItemsPtr - 1)), ItemSize);
    (*NumItemsPtr)--;
}

internal int64
IndexOfItemPointer(uint32 NumItems, void* Array, uint32 ItemSize, void* Item)
{
    return StringIndexOf(NumItems * ItemSize, (char*)Array, ItemSize, (char*)Item) / ItemSize;
}

#define RemoveItemFromArrayByValue(T, NumItems, Array, Item) RemoveItemFromArrayPointerByValue(&(NumItems), (void*)Array, sizeof(T), (void*)Item)
internal void
RemoveItemFromArrayPointerByValue(uint32* NumItemsPtr, void* Array, uint32 ItemSize, void* Item)
{
    int64 Index = IndexOfItemPointer(*NumItemsPtr, Array, ItemSize, Item);
    if (Index >= 0)
    {
        RemoveItemFromArrayPointerByIndex(NumItemsPtr, Array, ItemSize, (uint32)Index);
    }
}


//TODO(chronister): Hate myself
template <typename T>
struct array
{
    T* Values;
    uint32 Length;
    uint32 Capacity;
};

template <typename T>
array<T>
CreateArray()
{
    array<T> Result = {};
    Result.Values = AllocArray(T, 1);
    Result.Capacity = 1;
    return Result;
}

template <typename T>
uint32
PushArray(array<T>* Array, T Item)
{
    if (Array->Length > Array->Capacity - 1 || Array->Values == null)
    {
        uint32 OldCapacity = Array->Capacity;
        void* OldValues = Array->Values;
        Array->Capacity = Max(Array->Capacity + 10, Array->Capacity * 2);
        Array->Values = AllocArray(T, Array->Capacity);
        CopyMemory(Array->Values, OldValues, Array->Length * sizeof(T));
        Free(OldValues);
    }
    Array->Values[Array->Length] = Item;
    return Array->Length++;
}

template <typename T>
internal int64
SearchInArray(array<T> Array, bool32 (*Predicate)(T*, void*), void* Param = null)
{
    if (Array.Capacity == 0 || Array.Values == null || Array.Length == 0) { return -1; }
    for(uint32 i = 0;
        i < Array.Length;
        ++i)
    {
        T Item = Array.Values[i];
        if (Predicate(&Item, Param))
        {
            return i;
        }
    }
    return -1;
}

template <typename T>
T*
Get(array<T> Array, uint32 Index)
{
	if (Index < 0 || Index >= Array.Length) { return null; }
    return &Array.Values[Index];
}

template <typename T>
T*
GetLast(array<T> Array)
{
    return Get(Array, Array.Length - 1);
}

template <typename T>
uint32
RandomArrayIndex(array<T> Array)
{
    return rand() % Array.Length;
}

template<typename T>
T*
RandomArrayItem(array<T> Array)
{
    return &Array.Values[RandomArrayIndex(Array)];
}

template <typename T>
void
ClearArray(array<T> Array)
{
    for (uint32 i = 0; i < Array.Capacity; ++i)
    {
        Array.Values[i] = {};
    }
}

template <typename T>
void
FreeArray(array<T>* Array)
{
    Free(Array->Values);
    Array->Length = Array->Capacity = 0;
}

template <typename T>
void
RemoveItemByIndex(array<T>* Array, uint32 Index)
{
    if (Index < Array->Length)
    {
        if (Array->Length > 1)
            Array->Values[Index] = Array->Values[--Array->Length];
        else
            Array->Values[Index] = {};
    }
}

template <typename T>
bool32 PredicateItemsIdentical(T* ArrayItem, void* Comparison)
{
    return CompareMemory(ArrayItem, Comparison, sizeof(T));
}

template <typename T>
void
RemoveItemByValue(array<T>* Array, T* Item)
{
    int64 ItemIndex = SearchInArray<T>(*Array, PredicateItemsIdentical<T>, Item);
    if (ItemIndex >= 0)
    {
        RemoveItemByIndex(Array, (uint32)ItemIndex);
    }
}

#define CHR_ARRAY
#endif