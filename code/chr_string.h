#if !defined(CHR_STRING)

#include "stdarg.h" // For FormatString, consider moving into a more general output lib 
#include "chr.h"

void* Alloc(size_t BytesToAlloc, bool32 ZeroTheMemory);
bool32 Free(void* Memory);

struct string
{
    char* Value;
    size_t Length;
    size_t Capacity;
};

global_variable string EmptyString = {};

string
AllocateString(size_t Capacity, bool32 ZeroTheString = false)
{
    string Result;
    Result.Capacity = Capacity;
    Result.Value = (char*)Alloc(Result.Capacity, ZeroTheString);
    Result.Length = 0;
    return Result;
}

void
ExpandString(string* String, size_t NewCapacity)
{
    char* NewValue = (char*)Alloc(NewCapacity, false);
    
    CopyString(String->Capacity, String->Value, NewCapacity, NewValue, false);
    String->Capacity = NewCapacity;

    Free(String->Value);
    String->Value = NewValue;
}

void
CatStrings(string String1, string String2, string* Dest)
{
    CatStrings(String1.Length, String1.Value, String2.Length, String2.Value, Dest->Length, Dest->Value);
}

string 
CatStrings(string String1, string String2)
{
    string Result;
    Result.Length = String1.Length + String2.Length;
    Result.Capacity = Result.Length + 1;
    Result.Value = (char*)Alloc(Result.Capacity, false);
    CatStrings(String1, String2, &Result);
    return Result;
}

void
AppendToString(string* Main, string Addendum)
{
	Assert(Main->Length < Main->Capacity);
    CopyString(Addendum.Length, Addendum.Value, Main->Capacity - Main->Length - 1, Main->Value + Main->Length, true);
    Main->Length += Addendum.Length;
}

void
AppendFullToString(string* Main, string Addendum)
{
    if (Main->Length + Addendum.Length >= Main->Capacity)
    {
        char* MainOld = Main->Value;
        Main->Capacity = Max(Main->Length + Addendum.Length + 1, Main->Length * 2);
        Main->Value = (char*)Alloc(Main->Capacity, false);
        CopyString(Main->Length, MainOld, Main->Length, Main->Value, false);
    }
    AppendToString(Main, Addendum);
}

void 
CopyString(string Source, string* Dest)
{
    Assert(Dest->Capacity >= Source.Capacity);
    Assert(Dest->Capacity > Source.Length);
    CopyString(Source.Capacity-1, Source.Value, Dest->Capacity-1, Dest->Value);
    Dest->Length = Source.Length;
}

string
CopyString(string Source)
{
    string Dest;
    Dest.Length = Source.Length;
    Dest.Capacity = Source.Length + 1;
    Dest.Value = (char*)Alloc(Dest.Capacity, false);
    CopyString(Source.Length, Source.Value, Dest.Capacity, Dest.Value);
    return Dest;
}

internal char*
DuplicateCString(char* String)
{
	size_t Length = StringLength(String);
	char* NewString = (char*)Alloc(Length + 1, false);
	CopyString(Length, String, Length, NewString);
	NewString[Length] = 0;
	return NewString;
}

#define STR LoftCString

//NOTE(chronister): As the name implies, this assumes your string is null-terminated!
inline string 
LoftCString(char* CString, size_t Capacity, size_t Length)
{
    string Result;
    Result.Length = Length;
    Result.Capacity = Capacity;
    Result.Value = CString;
    return Result;
}

inline string 
LoftCString(char* CString, size_t Capacity)
{
    return LoftCString(CString, Capacity, StringLength(CString));
}

inline string
LoftCString(char* CString)
{
    size_t Length = StringLength(CString);
    return LoftCString(CString, Length + 1, Length);
}

void
FreeString(string* Str)
{
    Free(Str->Value);
    Str->Length = 0;
    Str->Capacity = 0;
    Str->Value = 0;
}


int64 
StringIndexOf(string Haystack, string Needle, int64 LowerBound = 0)
{
    return StringIndexOf(Haystack.Length, Haystack.Value, Needle.Length, Needle.Value, LowerBound);
}

int64 
StringLastIndexOf(string Haystack, string Needle, int64 UpperBound = -1)
{
    return StringLastIndexOf(Haystack.Length, Haystack.Value, Needle.Length, Needle.Value, UpperBound);
}

uint32 
StringOccurrences(string Source, string Search, uint32 StartIndex = 0)
{
    return StringOccurrences(Source.Length, Source.Value, Search.Length, Search.Value, StartIndex);
}

uint32
StringReplace(string Source, string* Dest, string Token, string Replacement, int StartIndex = 0, int OccurrencesToReplace = -1)
{
    uint32 OccurrenceCount = Min(StringOccurrences(Source, Token, StartIndex), (uint32)OccurrencesToReplace);
    if (OccurrenceCount > 0)
    {
        int64 Delta = (Replacement.Length - Token.Length) * OccurrenceCount; // Remember, might be negative
        size_t NewLength = Source.Length + Delta;

        Assert(Dest->Capacity > NewLength);

        StringReplace(Source.Length, Source.Value,
                      Dest->Length, Dest->Value,
                      Token.Length, Token.Value,
                      Replacement.Length, Replacement.Value
                      );

        Dest->Length = NewLength;
    }
    return OccurrenceCount;
}

uint32 
StringReplace(string* Source, string Token, string Replacement, 
            int StartIndex = 0, int OccurrencesToReplace = -1)
{
    uint32 OccurrenceCount = Min(StringOccurrences(*Source, Token, StartIndex), (uint32)OccurrencesToReplace);
    if (OccurrenceCount > 0)
    {
        int64 Delta = (Replacement.Length - Token.Length) * OccurrenceCount; // Remember, might be negative
        size_t NewLength = Source->Length + Delta;

        size_t DestLength = 0;
        size_t DestCapacity = NewLength + 1;
        char* DestValue = (char*)Alloc(DestCapacity, false);

        string Dest = STR(DestValue, DestCapacity, DestLength);
        StringReplace(*Source, &Dest,
                      Token, Replacement, 
                      StartIndex, OccurrencesToReplace);
		*Source = Dest;
    }
	else
	{
		//People expect this function to have allocated new memory.
		Source->Value = DuplicateCString(Source->Value);
	}

    return OccurrenceCount;
}

internal string
DuplicateString(string String)
{
    string NewString = {};
    NewString.Length = String.Length;
    NewString.Capacity = NewString.Length + 1;
    NewString.Value = DuplicateCString(String.Value);
    return NewString;
}


//TODO(chronister)
internal uint32
GetFormattedLengthVariadic(char* Format, va_list args)
{
    char Foo = 0;
    return vsnprintf(&Foo, 1, Format, args);
}

internal uint32
GetFormattedLength(char* Format, ...)
{
    va_list args;
    va_start(args, Format);
    uint32 Result = GetFormattedLengthVariadic(Format, args);
    va_end(args);

	return Result;
}

internal void
FormatIntoStringVariadic(string* Dest, char* Format, va_list args)
{
    va_list args2;

    va_copy(args2, args);
	uint32 FormattedLength = GetFormattedLengthVariadic(Format, args2);
    Dest->Length = Min(FormattedLength, Dest->Capacity - 1);
    va_end(args2);

    va_copy(args2, args);
    if (Dest->Length > 0)
    {
        vsnprintf(Dest->Value, Dest->Capacity, Format, args2);
    }

    va_end(args2);
}

internal void
FormatIntoString(string* Dest, char* Format, ...)
{
    va_list args;
    va_start(args, Format);
    FormatIntoStringVariadic(Dest, Format, args);
    va_end(args);
}

internal string
FormatStringVariadic(char* Format, va_list args)
{
    string Result = {};

    va_list args2;
    va_copy(args2, args);
    Result.Length = GetFormattedLengthVariadic(Format, args2);
    va_end(args2);

    va_copy(args2, args);
    Result.Capacity = Result.Length + 1;
    Result.Value = (char*)Alloc(Result.Capacity, false);
    FormatIntoStringVariadic(&Result, Format, args2);
    va_end(args2);

    return Result;
}

internal string
FormatString(char* Format, ...)
{
    string Result = {};
    va_list args;
    va_start(args, Format);
    Result = FormatStringVariadic(Format, args);
    va_end(args);
    return Result;
}

internal void
AppendFormatIntoString(string* Dest, char* Format, ...)
{
    va_list args;
    va_start(args, Format);
    string Temp = FormatStringVariadic(Format, args);
    va_end(args);

    AppendToString(Dest, Temp);
    FreeString(&Temp);
}

internal void
AppendFullFormatIntoString(string* Dest, char* Format, ...)
{
    va_list args;
    va_start(args, Format);
    string Temp = FormatStringVariadic(Format, args);
    va_end(args);

    AppendFullToString(Dest, Temp);
    FreeString(&Temp);
}

#define CHR_STRING
#endif
